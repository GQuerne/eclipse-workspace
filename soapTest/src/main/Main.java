package main;

import java.rmi.RemoteException;
import java.util.List;

import javax.xml.rpc.ServiceException;

import ws.*;

public class Main {

	public static void main(String[] args) {

		try {
			ProductImplService ProductsImplementsService = new ProductImplServiceLocator();
			IProduct product = ProductsImplementsService.getProductImplPort();
			System.out.println("First product id = " + product.findbyID(2).getId() + " and name = " + product.findbyID(2).getLibelle());
			System.out.println("All products "+ product.findAll());
			Product[] products = product.findAll(); 
			for (Product product2 : products) {
				System.out.println("All products "+ product2);
			}

		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		}

	}

}
