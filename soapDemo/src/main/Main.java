package main;

import javax.xml.ws.Endpoint;

import ws.DemoImpl;
import ws.ProductImpl;

public class Main {

    public static void main (String[] args) {

        try {

            Endpoint.publish("http://localhost:4789/ws/demo" , new DemoImpl());
            System.out.println("Done");
            
            Endpoint.publish("http://localhost:4790/ws/product" , new ProductImpl());

        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}