package entities;

public class Product {
	
	private int id;
	private String libelle, description;
	private double prix;
	
	public Product() {
	}
	
	public Product(int id, String libelle, String description, double prix) {
		this.id = id;
		this.libelle = libelle;
		this.description = description;
		this.prix = prix;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	
	@Override
	public String toString() {
		return "Product [id=" + id + ", libelle=" + libelle + ", description=" + description + ", prix=" + prix + "]";
	}
	
}
