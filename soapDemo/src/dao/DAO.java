package dao;

import java.sql.SQLException;
import java.util.List;

public interface DAO<T,K> {

	T findById(K uid)  throws ClassNotFoundException, SQLException;
	List<T> findAll()  throws ClassNotFoundException, SQLException;

}