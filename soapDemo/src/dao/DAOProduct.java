package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entities.Product;

public class DAOProduct implements DAO<Product, Integer> {

	static private String DBHOST = "jdbc:mysql://127.0.0.1/mag";
	static private String DBUSER = "root";
	static private String DBPSWD = "";

	public Product findById(Integer uid) {

		Product p = new Product();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM `product` WHERE `id`=?");
			ps.setInt(1, uid);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				p = new Product(rs.getInt("id"), rs.getString("libelle"), rs.getString("description"), rs.getInt("prix"));

			rs.close(); ps.close(); conn.close();

		} catch (ClassNotFoundException | SQLException err) {
			err.printStackTrace();
		}

		return p;
	}

	public List<Product> findAll() {

		List<Product> Product = new ArrayList();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM `product`");
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				Product.add(new Product(rs.getInt("id"), rs.getString("libelle"), rs.getString("description"), rs.getInt("prix")));

			rs.close(); ps.close(); conn.close();

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return Product;
	}

}