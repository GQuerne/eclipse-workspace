package ws;

import java.util.*;

import javax.jws.WebService;

import dao.DAOProduct;
import entities.Product;

@WebService(endpointInterface = ("ws.IProduct"))
public class ProductImpl implements IProduct{
	

	@Override
	public Product findbyID(int id) {
		DAOProduct daop = new DAOProduct();
		Product p = daop.findById(id);

		return p;
	}

	@Override
	public List<Product> findAll() {
		DAOProduct daop = new DAOProduct();
		List<Product> p = daop.findAll();
		
		return p;
	}

}
