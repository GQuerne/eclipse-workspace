package ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import entities.Product;

@WebService
public interface IProduct {		
	
	@WebMethod
	public Product findbyID(int id);

	@WebMethod
	public List<Product> findAll();
}