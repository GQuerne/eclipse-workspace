<%@page import="models.Eleve"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<% String curPage = "adds"; %>
	<meta charset="UTF-8">
	<title>Document</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<link rel="stylesheet" href="https://www.marc-grondin.com/java/servlet-style.css">

</head>
<body>

	<%@ include file="inc/nav.jsp" %>

	<div class="container">

		<form action="add" name="addChild" id="formChild" method="post">
			<fieldset>
				<ul class="list-unstyled">
					<li>
						<label for="first_name">First name*</label>
						<input type="text" name="first_name" id="first_name" value="" required="required">
					</li>
					<li>
						<label for="last_name">Last name*</label>
						<input type="text" name="last_name" id="last_name" value="" required="required">
					</li>
					<li>
						<label for="age">Age*</label>
						<input type="number" name="age" id="age" value="" required="required">
					</li>
					<li>
						<input type="submit" value="Add child">
					</li>
				</ul>
			</fieldset>
		</form>

	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

</body>
</html>