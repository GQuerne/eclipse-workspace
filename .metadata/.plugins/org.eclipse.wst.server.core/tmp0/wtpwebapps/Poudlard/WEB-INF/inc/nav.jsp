	<nav>
		<h1>Poudlard</h1>
		<ul class="list-unstyled list-inline">
			<li<%=(curPage.equals("home") ? " class=\"active\"" : "")%>><a href="index">Accueil</a></li>
			<li<%=(curPage.equals("houses") ? " class=\"active\"" : "")%>><a href="listHouses">voir Maisons</a></li>
			<li<%=(curPage.equals("students") ? " class=\"active\"" : "")%>><a href="listStudents">voir Eleves</a></li>
		</ul>
	</nav>