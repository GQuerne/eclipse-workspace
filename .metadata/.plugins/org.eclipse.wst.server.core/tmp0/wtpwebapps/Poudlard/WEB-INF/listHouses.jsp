<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="models.Eleve"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.*"%>
<%@page import="dao.DAOEleve"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="fr">
<head>
<%
	String curPage = "listHouses";
%>
<meta charset="UTF-8">
<title>Poudlard - Listing Houses</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<link rel="stylesheet" href="style.css">

</head>
<body>

	<%@ include file="inc/nav.jsp"%>

	<div class="container">
		<ul class="orphanage list-unstyled">
			<li class="orphanrow">
				<ul class="header list-unstyled list-inline">
					<li class="house">Nom</li>
					<li class="house">Score</li>
					<li class="house">Blason</li>
					<li class="house">Prof</li>
				</ul>
			</li>

			<c:forEach items="${maison}" var="m">
				<li class="orphanrow">
					<ul class="children list-unstyled list-inline">
						<li class="house">${m.nom}</li>
						<li class="house">${m.score}</li>
						<li class="house"><img alt="" src="${m.blason}"></li>
						<li class="house">${m.nomProf}</li>
					</ul>
				</li>
			</c:forEach>
		</ul>
	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
		integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
		integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
		crossorigin="anonymous"></script>

</body>
</html>