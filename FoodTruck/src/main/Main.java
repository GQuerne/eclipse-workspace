package main;

import javax.xml.ws.Endpoint;

import ws.NewsImpl;
import ws.ProduitImpl;

public class Main {

    public static void main (String[] args) {

        try {
        	Endpoint.publish("http://localhost:4791/ws/news" , new NewsImpl());
            System.out.println("news");
            Endpoint.publish("http://localhost:4792/ws/produit" , new ProduitImpl());
            System.out.println("produit");

        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}