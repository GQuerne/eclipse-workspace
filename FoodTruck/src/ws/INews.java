package ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import entities.News;

@WebService
public interface INews {		
	
	@WebMethod
	public News findNewsbyID(int id);

	@WebMethod
	public List<News> findAllNews();
}