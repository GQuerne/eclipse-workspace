package ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import entities.Produit;

@WebService
public interface IProduit {		
	
	@WebMethod
	public Produit findProdByID(int id);

	@WebMethod
	public List<Produit> findAllProd();
	
	@WebMethod
	public List<Produit> findAllWithParams(String cat, String sub, String search, String day);
	
	@WebMethod
	public List<Produit> findOrderBySales();
}