package ws;

import java.util.List;

import javax.jws.WebService;

import dao.DAONews;
import entities.News;

@WebService(endpointInterface = ("ws.INews"))
public class NewsImpl implements INews{
	
	@Override
	public News findNewsbyID(int id) {
		DAONews daon = new DAONews();
		News n = daon.findById(id);

		return n;
	}

	@Override
	public List<News> findAllNews() {
		DAONews daon = new DAONews();
		List<News> n = daon.findAll();
		
		return n;
	}

}
