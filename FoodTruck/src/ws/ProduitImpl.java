package ws;

import java.util.List;

import javax.jws.WebService;

import dao.DAOProduit;
import entities.Produit;

@WebService(endpointInterface = ("ws.IProduit"))
public class ProduitImpl implements IProduit{

	@Override
	public Produit findProdByID(int id) {
		DAOProduit daop = new DAOProduit();
		Produit p = daop.findById(id);

		return p;
	}

	@Override
	public List<Produit> findAllProd() {
		DAOProduit daop = new DAOProduit();
		List<Produit> p = daop.findAll();
		
		return p;
	}

	@Override
	public List<Produit> findAllWithParams(String cat, String sub, String search, String day) {
		DAOProduit daop = new DAOProduit();
		List<Produit> p = daop.findAllWithParams(cat, sub, search, day);
		
		return p;
	}

	@Override
	public List<Produit> findOrderBySales() {
		DAOProduit daop = new DAOProduit();
		List<Produit> p = daop.findOrderBySales();
		
		return p;
	}

}
