package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entities.Produit;

public class DAOProduit implements DAO<Produit, Integer> {

	static private String DBHOST = "jdbc:mysql://127.0.0.1/foodtruck";
	static private String DBUSER = "root";
	static private String DBPSWD = "";

	public Produit findById(Integer uid) {

		Produit p = new Produit();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM `produit` WHERE `id_produit`=?");
			ps.setInt(1, uid);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				p = new Produit(rs.getInt("id_produit"), rs.getString("nom"), rs.getString("img"), rs.getInt("prix"), rs.getInt("note"), rs.getInt("vente"), rs.getString("dispo"));

			rs.close(); ps.close(); conn.close();

		} catch (ClassNotFoundException | SQLException err) {
			err.printStackTrace();
		}

		return p;
	}
	
	@Override
	public List<Produit> findAll() {
		
		List<Produit> produits = new ArrayList();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM `produit`");
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				produits.add(new Produit(rs.getInt("id_produit"), rs.getString("nom"), rs.getString("img"), rs.getInt("prix"), rs.getInt("note"), rs.getInt("vente"), rs.getString("dispo")));

			rs.close(); ps.close(); conn.close();

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return produits;
	}
	
	public List<Produit> findAllWithParams(String cat, String sub, String search, String day) {

		List<Produit> produits = new ArrayList();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			String req = "SELECT * FROM produit";
			if(search != "") {
				req += " WHERE nom LIKE '%"+search+"%'";
			}
			PreparedStatement ps = conn.prepareStatement(req);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				produits.add(new Produit(rs.getInt("id_produit"), rs.getString("nom"), rs.getString("img"), rs.getInt("prix"), rs.getInt("note"), rs.getInt("vente"), rs.getString("dispo")));

			rs.close(); ps.close(); conn.close();

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return produits;
	}
	
	public List<Produit> findOrderBySales() {

		List<Produit> produits = new ArrayList();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM `produit` ORDER BY vente DESC LIMIT 0, 3");
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				produits.add(new Produit(rs.getInt("id_produit"), rs.getString("nom"), rs.getString("img"), rs.getInt("prix"), rs.getInt("note"), rs.getInt("vente"), rs.getString("dispo")));

			rs.close(); ps.close(); conn.close();

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return produits;
	}


}