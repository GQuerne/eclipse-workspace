package entities;

public class Produit {
	
	private int id_produit, note, vente;
	private String nom, img, dispo;
	private double prix;
	
	public Produit() {}

	public Produit(int id_produit, String nom, String img, double prix, int note, int vente, String dispo) {
		this.id_produit = id_produit;
		this.nom = nom;
		this.img = img;
		this.prix = prix;
		this.note = note;
		this.vente = vente;
		this.dispo = dispo;
	}

	public int getId_produit() {
		return id_produit;
	}
	public void setId_produit(int id_produit) {
		this.id_produit = id_produit;
	}

	public int getNote() {
		return note;
	}
	public void setNote(int note) {
		this.note = note;
	}

	public int getVente() {
		return vente;
	}
	public void setVente(int vente) {
		this.vente = vente;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}

	public String getDispo() {
		return dispo;
	}
	public void setDispo(String dispo) {
		this.dispo = dispo;
	}

	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}


	@Override
	public String toString() {
		return "Produit [id_produit=" + id_produit + ", note=" + note + ", vente=" + vente + ", nom=" + nom + ", img="
				+ img + ", dispo=" + dispo + ", prix=" + prix + "]";
	}

}
