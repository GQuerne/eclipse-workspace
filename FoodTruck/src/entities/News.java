package entities;

public class News {
	
	private int id_news;
	private String title, img, description;
	
	public News() {}

	public News(int id_news, String title, String img, String description) {
		this.id_news = id_news;
		this.title = title;
		this.img = img;
		this.description = description;
	}

	public int getId_news() {
		return id_news;
	}
	public void setId_news(int id_news) {
		this.id_news = id_news;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	
	@Override
	public String toString() {
		return "News [id_news=" + id_news + ", title=" + title + ", img=" + img + ", description=" + description + "]";
	}
	
}
