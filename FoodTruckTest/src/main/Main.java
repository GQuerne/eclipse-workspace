package main;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import ws.INews;
import ws.News;
import ws.NewsImplService;
import ws.NewsImplServiceLocator;
import ws.IProduit;
import ws.Produit;
import ws.ProduitImplService;
import ws.ProduitImplServiceLocator;

public class Main {

	public static void main(String[] args) {

		try {
			NewsImplService NewsImplementsService = new NewsImplServiceLocator();
			INews news = NewsImplementsService.getNewsImplPort();
			News[] n = news.findAllNews(); 
			for (News news2 : n) {
				System.out.println("All news "+ news2);
			}
			
			ProduitImplService ProduitsImplementsService = new ProduitImplServiceLocator();
			IProduit produit = ProduitsImplementsService.getProduitImplPort();
			//findByID
			System.out.println("Un produit id_produit = " + produit.findProdByID(2).getId_produit() + " son nom = " + produit.findProdByID(2).getNom());
			//findAll
			Produit[] produitAll = produit.findAllProd(); 
			for (Produit produits : produitAll) {
				System.out.println("Tous les produits "+ produits);
			}
			//findAllWithParams
			Produit[] produitSearch = produit.findAllWithParams(null, null, "salade", null);
			for (Produit produits : produitSearch) {
				System.out.println("recherche "+ produits);
			}
			//findOrderBySales
			Produit[] topSales = produit.findOrderBySales();
			for (Produit produits : topSales) {
				System.out.println("Meilleures ventes "+ produits);
			}
			

		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		}

	}

}
