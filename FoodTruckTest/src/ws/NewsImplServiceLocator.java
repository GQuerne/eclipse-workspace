/**
 * NewsImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws;

public class NewsImplServiceLocator extends org.apache.axis.client.Service implements ws.NewsImplService {

    public NewsImplServiceLocator() {
    }


    public NewsImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public NewsImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for NewsImplPort
    private java.lang.String NewsImplPort_address = "http://localhost:4791/ws/news";

    public java.lang.String getNewsImplPortAddress() {
        return NewsImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String NewsImplPortWSDDServiceName = "NewsImplPort";

    public java.lang.String getNewsImplPortWSDDServiceName() {
        return NewsImplPortWSDDServiceName;
    }

    public void setNewsImplPortWSDDServiceName(java.lang.String name) {
        NewsImplPortWSDDServiceName = name;
    }

    public ws.INews getNewsImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(NewsImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getNewsImplPort(endpoint);
    }

    public ws.INews getNewsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ws.NewsImplPortBindingStub _stub = new ws.NewsImplPortBindingStub(portAddress, this);
            _stub.setPortName(getNewsImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setNewsImplPortEndpointAddress(java.lang.String address) {
        NewsImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ws.INews.class.isAssignableFrom(serviceEndpointInterface)) {
                ws.NewsImplPortBindingStub _stub = new ws.NewsImplPortBindingStub(new java.net.URL(NewsImplPort_address), this);
                _stub.setPortName(getNewsImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("NewsImplPort".equals(inputPortName)) {
            return getNewsImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws/", "NewsImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws/", "NewsImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("NewsImplPort".equals(portName)) {
            setNewsImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
