package ws;

public class INewsProxy implements ws.INews {
  private String _endpoint = null;
  private ws.INews iNews = null;
  
  public INewsProxy() {
    _initINewsProxy();
  }
  
  public INewsProxy(String endpoint) {
    _endpoint = endpoint;
    _initINewsProxy();
  }
  
  private void _initINewsProxy() {
    try {
      iNews = (new ws.NewsImplServiceLocator()).getNewsImplPort();
      if (iNews != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iNews)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iNews)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iNews != null)
      ((javax.xml.rpc.Stub)iNews)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ws.INews getINews() {
    if (iNews == null)
      _initINewsProxy();
    return iNews;
  }
  
  public ws.News findNewsbyID(int arg0) throws java.rmi.RemoteException{
    if (iNews == null)
      _initINewsProxy();
    return iNews.findNewsbyID(arg0);
  }
  
  public ws.News[] findAllNews() throws java.rmi.RemoteException{
    if (iNews == null)
      _initINewsProxy();
    return iNews.findAllNews();
  }
  
  
}