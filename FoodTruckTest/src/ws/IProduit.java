/**
 * IProduit.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws;

public interface IProduit extends java.rmi.Remote {
    public ws.Produit[] findAllWithParams(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException;
    public ws.Produit findProdByID(int arg0) throws java.rmi.RemoteException;
    public ws.Produit[] findAllProd() throws java.rmi.RemoteException;
    public ws.Produit[] findOrderBySales() throws java.rmi.RemoteException;
}
