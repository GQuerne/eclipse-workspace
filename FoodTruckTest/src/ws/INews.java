/**
 * INews.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws;

public interface INews extends java.rmi.Remote {
    public ws.News findNewsbyID(int arg0) throws java.rmi.RemoteException;
    public ws.News[] findAllNews() throws java.rmi.RemoteException;
}
