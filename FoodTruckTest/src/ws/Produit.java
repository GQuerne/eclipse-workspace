/**
 * Produit.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws;

public class Produit  implements java.io.Serializable {
    private java.lang.String dispo;

    private int id_produit;

    private java.lang.String img;

    private java.lang.String nom;

    private int note;

    private double prix;

    private int vente;

    public Produit() {
    }

    public Produit(
           java.lang.String dispo,
           int id_produit,
           java.lang.String img,
           java.lang.String nom,
           int note,
           double prix,
           int vente) {
           this.dispo = dispo;
           this.id_produit = id_produit;
           this.img = img;
           this.nom = nom;
           this.note = note;
           this.prix = prix;
           this.vente = vente;
    }


    /**
     * Gets the dispo value for this Produit.
     * 
     * @return dispo
     */
    public java.lang.String getDispo() {
        return dispo;
    }


    /**
     * Sets the dispo value for this Produit.
     * 
     * @param dispo
     */
    public void setDispo(java.lang.String dispo) {
        this.dispo = dispo;
    }


    /**
     * Gets the id_produit value for this Produit.
     * 
     * @return id_produit
     */
    public int getId_produit() {
        return id_produit;
    }


    /**
     * Sets the id_produit value for this Produit.
     * 
     * @param id_produit
     */
    public void setId_produit(int id_produit) {
        this.id_produit = id_produit;
    }


    /**
     * Gets the img value for this Produit.
     * 
     * @return img
     */
    public java.lang.String getImg() {
        return img;
    }


    /**
     * Sets the img value for this Produit.
     * 
     * @param img
     */
    public void setImg(java.lang.String img) {
        this.img = img;
    }


    /**
     * Gets the nom value for this Produit.
     * 
     * @return nom
     */
    public java.lang.String getNom() {
        return nom;
    }


    /**
     * Sets the nom value for this Produit.
     * 
     * @param nom
     */
    public void setNom(java.lang.String nom) {
        this.nom = nom;
    }


    /**
     * Gets the note value for this Produit.
     * 
     * @return note
     */
    public int getNote() {
        return note;
    }


    /**
     * Sets the note value for this Produit.
     * 
     * @param note
     */
    public void setNote(int note) {
        this.note = note;
    }


    /**
     * Gets the prix value for this Produit.
     * 
     * @return prix
     */
    public double getPrix() {
        return prix;
    }


    /**
     * Sets the prix value for this Produit.
     * 
     * @param prix
     */
    public void setPrix(double prix) {
        this.prix = prix;
    }


    /**
     * Gets the vente value for this Produit.
     * 
     * @return vente
     */
    public int getVente() {
        return vente;
    }


    /**
     * Sets the vente value for this Produit.
     * 
     * @param vente
     */
    public void setVente(int vente) {
        this.vente = vente;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Produit)) return false;
        Produit other = (Produit) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dispo==null && other.getDispo()==null) || 
             (this.dispo!=null &&
              this.dispo.equals(other.getDispo()))) &&
            this.id_produit == other.getId_produit() &&
            ((this.img==null && other.getImg()==null) || 
             (this.img!=null &&
              this.img.equals(other.getImg()))) &&
            ((this.nom==null && other.getNom()==null) || 
             (this.nom!=null &&
              this.nom.equals(other.getNom()))) &&
            this.note == other.getNote() &&
            this.prix == other.getPrix() &&
            this.vente == other.getVente();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDispo() != null) {
            _hashCode += getDispo().hashCode();
        }
        _hashCode += getId_produit();
        if (getImg() != null) {
            _hashCode += getImg().hashCode();
        }
        if (getNom() != null) {
            _hashCode += getNom().hashCode();
        }
        _hashCode += getNote();
        _hashCode += new Double(getPrix()).hashCode();
        _hashCode += getVente();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Produit.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/", "produit"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dispo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dispo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_produit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id_produit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("img");
        elemField.setXmlName(new javax.xml.namespace.QName("", "img"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("note");
        elemField.setXmlName(new javax.xml.namespace.QName("", "note"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prix");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	@Override
	public String toString() {
		return "Produit [dispo=" + dispo + ", id_produit=" + id_produit + ", img=" + img + ", nom=" + nom + ", note="
				+ note + ", prix=" + prix + ", vente=" + vente + ", __equalsCalc=" + __equalsCalc + ", __hashCodeCalc="
				+ __hashCodeCalc + "]";
	}
    
    

}
