/**
 * NewsImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws;

public interface NewsImplService extends javax.xml.rpc.Service {
    public java.lang.String getNewsImplPortAddress();

    public ws.INews getNewsImplPort() throws javax.xml.rpc.ServiceException;

    public ws.INews getNewsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
