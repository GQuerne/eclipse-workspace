/**
 * ProduitImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws;

public class ProduitImplServiceLocator extends org.apache.axis.client.Service implements ws.ProduitImplService {

    public ProduitImplServiceLocator() {
    }


    public ProduitImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ProduitImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ProduitImplPort
    private java.lang.String ProduitImplPort_address = "http://localhost:4792/ws/produit";

    public java.lang.String getProduitImplPortAddress() {
        return ProduitImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ProduitImplPortWSDDServiceName = "ProduitImplPort";

    public java.lang.String getProduitImplPortWSDDServiceName() {
        return ProduitImplPortWSDDServiceName;
    }

    public void setProduitImplPortWSDDServiceName(java.lang.String name) {
        ProduitImplPortWSDDServiceName = name;
    }

    public ws.IProduit getProduitImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ProduitImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getProduitImplPort(endpoint);
    }

    public ws.IProduit getProduitImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ws.ProduitImplPortBindingStub _stub = new ws.ProduitImplPortBindingStub(portAddress, this);
            _stub.setPortName(getProduitImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setProduitImplPortEndpointAddress(java.lang.String address) {
        ProduitImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ws.IProduit.class.isAssignableFrom(serviceEndpointInterface)) {
                ws.ProduitImplPortBindingStub _stub = new ws.ProduitImplPortBindingStub(new java.net.URL(ProduitImplPort_address), this);
                _stub.setPortName(getProduitImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ProduitImplPort".equals(inputPortName)) {
            return getProduitImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws/", "ProduitImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws/", "ProduitImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ProduitImplPort".equals(portName)) {
            setProduitImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
