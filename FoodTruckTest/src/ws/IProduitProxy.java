package ws;

public class IProduitProxy implements ws.IProduit {
  private String _endpoint = null;
  private ws.IProduit iProduit = null;
  
  public IProduitProxy() {
    _initIProduitProxy();
  }
  
  public IProduitProxy(String endpoint) {
    _endpoint = endpoint;
    _initIProduitProxy();
  }
  
  private void _initIProduitProxy() {
    try {
      iProduit = (new ws.ProduitImplServiceLocator()).getProduitImplPort();
      if (iProduit != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iProduit)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iProduit)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iProduit != null)
      ((javax.xml.rpc.Stub)iProduit)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ws.IProduit getIProduit() {
    if (iProduit == null)
      _initIProduitProxy();
    return iProduit;
  }
  
  public ws.Produit[] findAllWithParams(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException{
    if (iProduit == null)
      _initIProduitProxy();
    return iProduit.findAllWithParams(arg0, arg1, arg2, arg3);
  }
  
  public ws.Produit findProdByID(int arg0) throws java.rmi.RemoteException{
    if (iProduit == null)
      _initIProduitProxy();
    return iProduit.findProdByID(arg0);
  }
  
  public ws.Produit[] findAllProd() throws java.rmi.RemoteException{
    if (iProduit == null)
      _initIProduitProxy();
    return iProduit.findAllProd();
  }
  
  public ws.Produit[] findOrderBySales() throws java.rmi.RemoteException{
    if (iProduit == null)
      _initIProduitProxy();
    return iProduit.findOrderBySales();
  }
  
  
}